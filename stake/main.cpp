// mian.cpp

#include <QApplication>
#include <QTabWidget>
#include <QProcess>
#include <QLibrary>

typedef QWidget Plugin;
typedef QList<Plugin *> PluginList;
typedef QWidget *(*GetPlugin)();

PluginList loadPlugins()
{
    PluginList plugs;
    QString plugdir = "/home/x/tmp/learn/plugin-model/build-plugin";

    QProcess process;
    process.start(QString("sh -c \"ls %1/*.so.[0-9]*.[0-9]*.[0-9]*\"").arg(plugdir));
    process.waitForFinished();
    QString str = process.readAllStandardOutput();

    QStringList names = str.split(QRegExp("[\\s\\n]+"), QString::SkipEmptyParts);

    foreach (QString name, names)
    {
        QLibrary factory(name);
        if (factory.load())
        {
            GetPlugin getPlugin = (GetPlugin)factory.resolve("getPlugin");

            if (getPlugin)
            {
                QWidget *page = getPlugin();
                plugs << page;
            }
        }
        else
        {
            printf("Can not load library %s.\n", name.toStdString().c_str());
            fflush(stdout);
        }
    }

    return plugs;
}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QTabWidget tab;

    foreach (Plugin *page, loadPlugins())
    {
        tab.addTab(page, page->objectName());
    }

    tab.show();

    return app.exec();
}
