// main.h
#ifndef MAINXXX_H
#define MAINXXX_H

#include <QWidget>
#include <QLabel>

class Page1 : public QWidget
{
    Q_OBJECT

public:
    Page1(QWidget *parent = 0) : QWidget(parent)
    {
        QLabel *label = new QLabel("plugin page.", this);
        setObjectName("page1");
    }

    ~Page1() {}

    const char *name() {return objectName().toStdString().c_str();}
};

#endif
